#!/bin/bash

cd /home/webgoat 
service nginx start
sleep 1
echo "Starting WebGoat..."

java \
 -Duser.home=/home/webgoat \
 -Dfile.encoding=UTF-8 \
 --add-opens java.base/java.util=ALL-UNNAMED \
 --add-opens java.base/java.lang.reflect=ALL-UNNAMED \
 --add-opens java.base/java.text=ALL-UNNAMED \
 --add-opens java.desktop/java.awt.font=ALL-UNNAMED \
 --add-opens java.base/sun.nio.ch=ALL-UNNAMED \
 --add-opens java.base/java.io=ALL-UNNAMED \
 -jar webgoat.jar --webgoat.build.version="$1" --server.address=127.0.0.1> webgoat.log &

sleep 10

echo "Starting WebWolf..."

echo "Browse to http://localhost to get started" >> webgoat.log

tail -300f webgoat.log
